#!/bin/env python3
"""
Plot the angle of a PDL over time
"""
import ithildin as ith
import numpy as np
import matplotlib.pyplot as plt
import re
import sys
from figure_helper import *
from multiple_locator import multiple_formatter
from itertools import chain
from scipy.spatial.distance import cdist
PDL = ith.PDL

def merge_pdls(self, other:PDL):
    ordered = self.ordered and other.ordered
    assert(np.allclose(self.res, other.res))
    o0, o1 = 1, 1
    if ordered:
        self_points = self.points
        other_points = other.points
        c = cdist([self_points[0], self_points[-1]], [other_points[0], other_points[-1]])
        o0, o1 = np.unravel_index(np.argmin(c, axis=None), (2, 2))
        o0 = 2*o0 - 1
        o1 = 1 - 2*o1
    pts = np.concatenate([self.points_grid[::o0], other.points_grid[::o1]], axis=0)
    return PDL(pts, res=self.res, time=self.time, ordered=ordered)

def read_pdl_trajectory(filename_tra:str, filename_idx:str=None, dt:float=1.):
    with open(filename_tra, "r") as f:
        trajectories = eval(f.read())

    if filename_idx:
        traj_idx = []
        with open(filename_idx, "r") as f:
            for line in f:
                traj_idx.append(int(line))

        trajectories = [trajectories[i] for i in traj_idx]

        pdls = sorted(chain(*trajectories), key=lambda pdl: pdl.time)
        trajectory = [pdls[0]]
        for pdl in pdls[1:]:
            if abs(pdl.time - trajectory[-1].time) < 0.5*dt:
                pdls.append(merge_pdls(pdl, trajectory.pop()))
            else:
                trajectory.append(pdl)
    else:
        trajectory = trajectories[0]

    # ignore beginning of trajectory
    trajectory = trajectory[len(trajectory)//5:]

    times = np.array([pdl.time for pdl in trajectory])
    lengths = np.array([pdl.arc_length() for pdl in trajectory])
    betas_and_ells = [pdl.angle(return_ellipticity=True) for pdl in trajectory]
    betas = np.unwrap([beta*2 for beta, _ in betas_and_ells])/2
    # betas = np.array([beta for beta, _ in betas_and_ells])
    ells = np.array([ell for _, ell in betas_and_ells])

    sel = ells > 0.9

    Lmax, Lmin = np.nanmax(lengths), np.nanmin(lengths)
    sel *= lengths > Lmin + 0.1*(Lmax - Lmin)

    ddL = np.roll(lengths, 1) - 2*lengths + np.roll(lengths, -1)
    ddL[0] = ddL[-1] = 0
    sel *= ddL < 0.5*np.nanmax(ddL)

    times = times[sel]
    betas = betas[sel]
    lengths = lengths[sel]
    ells = ells[sel]

    return times, betas, lengths, ells

if __name__ == "__main__":
    _ = sys.argv[0]
    tras = sys.argv[1:-1:2]
    tra_idxs = sys.argv[2:-1:2]
    outfile = sys.argv[-1]

    fig, axes = subplots(1, len(tras), figsize=(maxwidthin, 6*cm), set_aspect=False)

    for (i, ax), tra, tra_idx in zip(enumerate(axes), tras, tra_idxs):

        sd, _, _ = load_simdata_from_filename(tra)
        use_units = not "Aliev-Panfilov" in sd.log.modeltype
        dt, dz, dy, dx = sd.deltas

        times, betas, lengths, ells = read_pdl_trajectory(tra, tra_idx, dt=dt)

        beta_fit = np.poly1d(np.polyfit(times, betas, deg=1))
        omega_mean = np.mean(beta_fit.deriv()(times))
        omegas = (betas[1:] - betas[:-1])/sd.log.dt
        tau = 2*np.pi

        # tim = times - times[0]
        # fig, ax = plt.subplots(figsize=(2.7,2.7), subplot_kw=dict(projection="polar"))
        # ax.plot(betas, lengths, lw=0)
        # ith.plot.colorline(betas, lengths, times, ax=ax, cmap="RdPu")
        # fig.savefig(outfile)

        ax.plot(times, beta_fit(times), c="k", ls=":")
        ax.plot(times, betas, c="tab:orange", ls="-")

        # ith.plot.periodic_plot(times, betas)
        # t = np.linspace(times[0], times[-1], 40)
        # ith.plot.periodic_plot(t, beta_fit(t))
        # plt.ylim(0, tau)
        # ax.set_ylabel("angle of PDL")

        ymin, ymax = ax.get_ylim()
        fac = 2**(int(np.log2((ymax - ymin)/np.pi)) - 2)

        ax.set_ylabel("angle $\\beta$ of PDL in rad")
        if fac < 2**-3:
            pass
            # fac = 2**-1
            # ymid = (ymax + ymin)/2
            # ymid = (np.pi/2)*np.round(ymid/(np.pi/2))
            # ax.set_ylim(ymid - np.pi/2, ymid + np.pi/2)
            # ax.set_yticks([ymid - np.pi/2, ymid, ymid + np.pi/2])
        else:
            ax.yaxis.set_major_locator(plt.MultipleLocator(fac*np.pi))
            ax.yaxis.set_minor_locator(plt.NullLocator())
            ax.yaxis.set_major_formatter(plt.FuncFormatter(multiple_formatter(100)))

        ax.set_xlabel("time $t$" + (" in ms" if use_units else ""))
        xlim = ax.get_xlim()
        ax.set_xticks(xlim)
        ax.set_xticklabels([f"{int(x):d}" for x in xlim])

        abbr = modelabbr[sd.log.modeltype]
        ax.set_title(f"({sublabel(i)}) {abbr} {'experiment' if abbr == 'OM' else 'simulation'}")

    plt.tight_layout()
    fig.savefig(outfile)
