#!/bin/env python3
"""
A figure showing steps to get to PDLs.
"""
from figure_helper import *
import numpy as np
import matplotlib.pyplot as plt
import sys
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes, mark_inset

outfile = sys.argv[1]
data = [load_simdata_from_filename(a) for a in sys.argv[2:]]

rhomin = 0
# rhomax = max(np.nanmax(u) for sd, u, varname in data[3:])
rhomax = 1

Nrow = len(data)//3
fig, axes = subplots(Nrow, 3, figsize=(maxwidthin, Nrow*6*cm), sharey="all", sharex="all")

for ((i, ax), (sd, u, varname)) in zip(enumerate(axes), data):
    print(f"{outfile}: {varname}")
    x, y = sd.meshgrid(-1, -2)
    ufr = np.where(sd.mask[0], u[-20,0], np.nan)

    lims = ((71-5,71+5),(57-5,57+5))
    locs = (3, 2, 4)
    axins = zoomed_inset_axes(ax, 5, loc=locs[0])
    axins.set_xlim(lims[0])
    axins.set_ylim(lims[1])
    axins.set_xticks([])
    axins.set_yticks([])
    mark_inset(ax, axins, loc1=locs[1], loc2=locs[2], fc="none", ec=(0.5, 0.5, 0.5, 1.), lw=0.5)
    subaxes = [ax, axins]

    for a in subaxes:
        if varname == "u":
            qm = a.pcolormesh(x, y, ufr, shading="nearest", cmap="inferno", vmin=UMIN, vmax=UMAX)
        else:
            qm = a.pcolormesh(x, y, ufr, shading="nearest", cmap="Greens", vmin=rhomin, vmax=rhomax)
    ax.set_title(f"({sublabel(i)}) ${label2formula(varname)}$ at SNR = {sd.log.serialnr}~dB")

plt.tight_layout()

add_colorbar_u(fig, axes[:3])
add_colorbar(fig, axes[3:], ticks=[rhomin, rhomax], cmap="Greens")

plt.savefig(outfile)
