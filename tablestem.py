#!/bin/env python3
"""
Use data in log file to determine params for phase and phase defects.
"""
import csv
import numpy as np
import matplotlib.pyplot as plt
import sys
import ithildin as ith

def read_parameters_for_model(tablefile, model):
    with open(tablefile, newline="") as f:
        reader = csv.DictReader(f, delimiter=";", quotechar="'", quoting=csv.QUOTE_NONNUMERIC)
        for row in reader:
            if row["model"] == model:
                return row
    raise Exception(f"Model '{model}' not found in csv file.")

if __name__ == "__main__":
    assert(len(sys.argv) >= 3)
    logfile, tablefile = sys.argv[1:3]
    modeltype = ith.Log(logfile).modeltype
    params = read_parameters_for_model(tablefile, modeltype)
    sel = dict()
    for a in sys.argv[3:]:
        p = a.split("=")
        sel[p[0]] = params[p[-1]]

    if "u_iso" in sel and isinstance(sel["u_iso"], str):
        sel["u_iso"] = list(float(u) for u in sel["u_iso"].split(","))

    print(sel)
    print(sel, file=sys.stderr)
