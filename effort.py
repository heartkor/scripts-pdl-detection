#!/bin/env python3
"""
Measure computation time for one simulation
"""
import datetime
import numpy as np
import matplotlib.pyplot as plt
import ithildin as ith
import yaml

def measure_effort(sd:ith.SimData):
    print(yaml.safe_dump({f"N{c}": dx for c, dx in zip("tzyx", sd.shape)}))

    for algo in ["edge_cos", "edge_glat", "edge_rpg", "edge_cpg", "edge_pc", "di", "svf", "topo", "edge_ip"]:
        algo = algo.split("_")
        edge = len(algo) > 1
        algo = algo[-1]

        start = datetime.datetime.now()
        if edge:
            sd.phase_edge_defect(algo)
        else:
            sd.phase_defect(algo)
        end = datetime.datetime.now()

        duration = end - start
        print(("sigma" if edge else "rho") + f" {algo}: {duration.total_seconds()}")

if __name__ == "__main__":
    sd = ith.SimData.from_stem("phase_68")
    measure_effort(sd)
