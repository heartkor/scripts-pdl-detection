#!/bin/env python3
'''
Prepare log file for noisy data
'''
import ithildin as ith
import sys

if __name__ == '__main__':
    infile = sys.argv[1]
    SNRdB = int(sys.argv[2])
    outfile = sys.argv[3]

    log = ith.Log(infile)

    series, serialnr = outfile.split('_')[:2]
    log.modeltype = "noisy " + log.modeltype
    log.simseries = series
    log.serialnr = int(serialnr)
    log.full_data["SNRdB"] = SNRdB

    with open(outfile, 'w') as f:
        f.write(log.yaml)
