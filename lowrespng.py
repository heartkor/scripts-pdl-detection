#!/bin/env python3
'''
Plot similarity of lower resolution phase defect fields rho
'''
import numpy as np
import matplotlib.pyplot as plt
import ithildin as ith
import sys

if __name__ == '__main__':
    sds = dict()
    varname = None
    for varfilename in sys.argv[1:-1]:
        series, number, varname = varfilename.split('_')
        number = int(number)
        varname = varname.split('.')[0]
        sds[number] = ith.SimData.from_stem(f'{series}_{number}')
    nmin = min(sds.keys())
    nmax = max(sds.keys())

    fig, (ax0, ax1) = plt.subplots(1, 2, figsize=(12, 5))

    # vmax = max(np.nanmax(sd.vars[varname][-1]) for sd in sds.values())
    vmax = None

    interpolation = 'bilinear'
    interpolation = None

    sds[nmin].plot(varname, it=-1, ax=ax0, interpolation=interpolation, cmap='Greens', vmin=0, vmax=vmax)
    sds[nmax].plot(varname, it=-1, ax=ax1, interpolation=interpolation, cmap='Greens', vmin=0, vmax=vmax)

    for ax in [ax0, ax1]:
        ax.set_aspect(1)
        ax.set_xlim(100, 180)
        ax.set_ylim(175, 255)
    plt.tight_layout()
    fig.savefig(sys.argv[-1])
