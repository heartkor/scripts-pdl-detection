#!/bin/env python3
"""
Reduce resolution of a varfile
"""
import ithildin as ith
import sys
import numpy as np

# def reduce(v:np.ndarray, N:int) -> np.ndarray:
#     return v[:,::N,::N,::N]

def reduce(v:np.ndarray, N:int) -> np.ndarray:
    shape = list(v.shape)
    shape[2] //= N
    shape[3] //= N

    vsum = np.zeros(shape, dtype=v.dtype)
    count = np.zeros(shape, dtype=int)
    for iy in range(N):
        for ix in range(N):
            view = v[:,:,iy::N,ix::N][:,:,:shape[2],:shape[3]]
            finite = np.isfinite(view)
            vsum[finite] += view[finite]
            count += finite
    return vsum/count

if __name__ == "__main__":
    assert(len(sys.argv) in [4, 5])

    v = ith.varfile.read_var(sys.argv[1])

    if len(sys.argv) == 5:
        inhom = ith.varfile.read_var(sys.argv[2], dtype=np.int32)
        assert(inhom.shape[1:] == v.shape[1:])
        v = np.where(inhom, v, np.nan)

    N = int(sys.argv[-2])
    np.save(sys.argv[-1], reduce(v, N))
