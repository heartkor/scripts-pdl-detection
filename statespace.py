#!/bin/env python3
"""
Draw a historgram of the state space.
"""
import csv
import numpy as np
import matplotlib.pyplot as plt
import sys
import ithildin as ith
from tablestem import read_parameters_for_model

def histogram_state_space(u, v, ax=None, u_iso=0.5, v_iso=0.5, skewp0=0., skewp1=1.):
    if ax is None:
        ax = plt.gca()

    dens, uh, vh = np.histogram2d(u, v, bins=200)
    u_min, u_max = uh[0], uh[-1]
    v_min, v_max = vh[0], vh[-1]
    uh, vh = np.meshgrid(uh, vh)
    dens = np.log(dens.T)

    qm = ax.pcolormesh(uh, vh, dens, cmap="Greens")

    li = ax.plot(u_iso, v_iso, "go")

    choice = np.random.randint(u.size, size=1000)
    up, vp = u[choice], v[choice]
    phase = ith.phase.skew(ith.phase.phase_state_space(up, vp, u_iso=u_iso, v_iso=v_iso), p0=skewp0, p1=skewp1)
    sc = ax.scatter(up, vp, c=phase, cmap="twilight", marker=".")

    ax.set_xlim(u_min, u_max)
    ax.set_ylim(v_min, v_max)
    ax.set_title("log density in state space")
    ax.set_xlabel("first variable $u$")
    ax.set_ylabel("second variable $v$")
    return qm, li, sc

if __name__ == "__main__":
    assert(len(sys.argv) >= 4)
    stem, tablefile, outfile = sys.argv[1:4]
    sd = ith.SimData.from_stem(stem)
    params = read_parameters_for_model(tablefile, sd.log.modeltype)

    u = sd.vars[params["V"]]
    if params["R"][:2] == "1-":
        v = 1 - sd.vars[params["R"][2:]]
    else:
        v = sd.vars[params["R"]]

    params = dict(
            u_iso=float(params["Vstar"]),
            v_iso=float(params["Rstar"]),
            skewp0=float(params["p0"]),
            skewp1=float(params["p1"]),
    )

    fig, ax = plt.subplots(dpi=180)
    histogram_state_space(u.flatten(), v.flatten(), ax=ax, **params)
    fig.savefig(outfile)
