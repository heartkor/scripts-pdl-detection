#!/bin/env python3
"""
A figure showing steps to get to PDLs.
"""
from figure_helper import *
import numpy as np
import matplotlib.pyplot as plt
import sys

outfile = sys.argv[1]
data = [load_simdata_from_filename(a) for a in sys.argv[2:]]

Nrow = len(data)//3
fig, axes = subplots(Nrow, 3, figsize=(maxwidthin, Nrow*6*cm), sharey="row")

pairs = [(data[0], data[2]), (data[0], data[1]), (data[1], data[2])]

for ((i, ax), ((sd, u, uname), (_, v, vname))) in zip(enumerate(axes), pairs):
    print(f"{outfile}: {uname} {vname}")
    uf, vf = u[-20,0].flatten(), v[-20,0].flatten()

    if sorted([uname, vname]) == ["phiact", "phiskew"]:
        line = np.transpose([uf, vf])
        line = line[np.argsort(uf)]
        ax.plot(*line.T, lw=4)

    else:
        dens, uh, vh = np.histogram2d(uf, vf, bins=np.linspace(0, 2*np.pi, 200))
        uh, vh = np.meshgrid(uh, vh)
        dens = np.log(dens)
        qm = ax.pcolormesh(uh, vh, dens.T, cmap="Blues")

    ax.text(0.05, 0.95, f"({sublabel(i)})", color="k", horizontalalignment="left", verticalalignment="top", transform=ax.transAxes)
    ax.set_xlim(0, 2*np.pi)
    ax.set_ylim(0, 2*np.pi)
    ax.set_xlabel(f"${label2formula(uname)}$")
    ax.set_ylabel(f"${label2formula(vname)}$")

    ax.set_xticks([0, np.pi, 2*np.pi])
    ax.set_xticklabels(["0", "$\\pi$", "$2\\pi$"])
    ax.set_yticks([0, np.pi, 2*np.pi])
    ax.set_yticklabels(["0", "$\\pi$", "$2\\pi$"])

axes[-1].plot([0, 2*np.pi], [0, 2*np.pi], "k:")

plt.tight_layout()
plt.savefig(outfile)
