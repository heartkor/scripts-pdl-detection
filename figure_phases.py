#!/bin/env python3
"""
A figure showing steps to get to PDLs.
"""
from figure_helper import *
import numpy as np
import matplotlib.pyplot as plt
import sys

outfile = sys.argv[1]
data = [load_simdata_from_filename(a) for a in sys.argv[2:]]

Nrow = len(data)//3
fig, axes = subplots(Nrow, 3, figsize=(maxwidthin, Nrow*6*cm), sharey="row")

for ((i, ax), (sd, u, varname)) in zip(enumerate(axes), data):
    print(f"{outfile}: {varname}")
    x, y = sd.meshgrid(-1, -2)
    qm = ax.pcolormesh(x, y, np.where(sd.mask[0], u[-20,0], np.nan), shading="nearest", cmap="twilight", vmin=0, vmax=2*np.pi)
    # ax.text(0.05, 0.95, f"({sublabel(i)}) {modelabbr[sd.log.modeltype]} ${label2formula(varname)}$", color="k", backgroundcolor=(1,1,1,.5), horizontalalignment="left", verticalalignment="top", transform=ax.transAxes)
    ax.set_title(f"({sublabel(i)}) {modelabbr[sd.log.modeltype]} ${label2formula(varname)}$")

for ax in axes[:-3]:
    ax.set_xlabel(None)
    # ax.xaxis.set_visible(False)

plt.tight_layout()

plt.savefig(outfile)
