#!/bin/env python3
"""
Find and analyse the trajectories of PDLs
"""
import ithildin as ith
import numpy as np
import sys
from figure_helper import load_simdata_from_filename

sd, rho, _ = load_simdata_from_filename(sys.argv[1])
_, phi, _ = load_simdata_from_filename(sys.argv[2])
Nt, Nz, Ny, Nx = sd.shape
dt, dz, dy, dx = sd.deltas
it_ = max([Nt//2, Nt-300])
t = sd.linspace(0)
x = sd.linspace(3)
y = sd.linspace(2)
phi = sd.vars["phi"] = np.where(sd.mask, phi, 0)
rho = sd.vars["rho"] = np.where(sd.mask, rho, 0)
rho_iso = 0.1

pdls = []
for it in np.arange(it_, Nt):
    pdls_ = ith.pdl.pdls_ridge(rho[it,0], x, y, dx=dx, dy=dy, threshold=rho_iso, time=t[it])
    for pdl in pdls_:
        # if len(pdl) > 5:
            pdl.store_nearby_phase(phi[it,0])
            pdls.append(pdl)

trajectories = ith.pdl.find_trajectories(pdls, maxdist=10)
trajectories.sort(key=lambda t: -len(t))

print(f"found {len(trajectories)} trajectories, ranging from {len(trajectories[-1])} to {len(trajectories[0])} points")

with open(sys.argv[3], "w") as f:
    f.write(repr(trajectories))
