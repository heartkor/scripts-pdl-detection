#!/bin/env python3
"""
Calculate APD for a simulation
"""
import numpy as np
import matplotlib.pyplot as plt
import ithildin as ith
import sys

def calc_apd(sd:ith.SimData):
    try:
        iso = sd.log.tiplineisovalues[0]
    except:
        iso = 0.5
    t = sd.times

    apd, tact, tdec = ith.pulse.duration(sd.vars["u"], u_iso=iso, t=t)
    apd[~np.isfinite(apd)] = np.nan

    density, bins = np.histogram(apd[apd>0].flatten(), bins=100, density=True)
    centers = (bins[1:] + bins[:-1])/2

    apd_peak = centers[np.argmax(density)]
    print(f"apd_peak = {apd_peak}")

    apd_mean = np.average(apd[apd>0])
    print(f"apd_mean = {apd_mean}")

    apd_median = np.median(apd[apd>0])
    print(f"apd_median = {apd_median}")

    apd_max = centers[-1]
    print(f"apd_max = {apd_max}")

    plt.bar(bins[:-1], density, bins[1:] - bins[:-1], color="lightgray", label="histogram")
    plt.plot(apd_peak, 0, "og", label=f"apd_peak = {apd_peak}")
    plt.plot(apd_mean, 0, "ob", label=f"apd_mean = {apd_mean}")
    plt.plot(apd_median, 0, "oc", label=f"apd_median = {apd_median}")
    plt.plot(apd_max, 0, "or", label=f"apd_max = {apd_max}")
    plt.xlabel("APD")
    plt.ylabel("probability density")
    plt.legend()

if __name__ == "__main__":
    filename = sys.argv[1]
    plotname = sys.argv[2] if len(sys.argv) > 2 else None
    stem = "_".join(filename.split("_")[:2])
    sd = ith.SimData.from_stem(stem)
    calc_apd(sd)
    if plotname:
        plt.savefig(plotname)
    else:
        plt.show()
