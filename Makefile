#!/bin/make
# Calculate phases and phase defect fields for simulations

# logfiles := $(wildcard *_log.txt)
logfiles := optical_20200204114234_log.txt pdl2D_10_log.txt phase_68_log.txt phasealpa_7_log.txt
stateplots := $(logfiles:_log.txt=_statespace.png)
apdplots := $(logfiles:_log.txt=_apd.png)
phases := $(logfiles:_log.txt=_phiact.npy) $(logfiles:_log.txt=_phiarr.npy) $(logfiles:_log.txt=_phiskew.npy)
rhos := $(phases:.npy=rhorpg.npy) $(phases:.npy=rhocpg.npy) $(phases:.npy=rhodipole.npy) $(phases:.npy=rhomph.npy) $(phases:.npy=rhocos.npy) $(phases:.npy=rhopc.npy) $(phases:.npy=rhosvf.npy) $(phases:.npy=rhoip.npy)
rhos := $(rhos) $(logfiles:_log.txt=_rhoglat.npy) $(logfiles:_log.txt=_rhotopo.npy)
sigmas := $(phases:.npy=sigma3rpg.npy) $(phases:.npy=sigma3cpg.npy) $(phases:.npy=sigma3ip.npy) $(phases:.npy=sigma3pc.npy) $(phases:.npy=sigma3cos.npy)
sigmas := $(sigmas) $(logfiles:_log.txt=_sigma3glat.npy)
sigmas := $(sigmas) $(shell echo $(sigmas) | sed 's/sigma3/sigma2/g')
# movies := $(shell ls *_{u,phiact,phiarr,phiskew}.npy)
# movies := $(shell ls *.npy)
# movies := $(movies:npy=mp4)
movies := optical_20200204114234_phiarr.mp4 optical_20200204114234_phiarrrhopc.mp4 optical_20200204114234_phiskew.mp4 optical_20200204114234_phiskewrhopc.mp4 optical_20200204114234_u.mp4 phase_68_phiarr.mp4 phase_68_phiarrrhopc.mp4 phase_68_phiskew.mp4 phase_68_phiskewrhopc.mp4 phase_68_u.mp4
plots := $(wildcard *.npy)
plots := $(plots:npy=png)
trajectories := trajectories_angle.pdf trajectories_length.pdf
figures := figure_phases_corr.png figure_phases.png figure_process.png figure_sims.png figure_u.png figure_noise.png figure_lowres.png $(logfiles:%_log.txt=figure_defects_%.png)  figure_groundtruth_forcePDLlowres_62.png figure_characteristic_optical_20200204114234_phiarrrhopc.png
targets := $(apdplots) $(stateplots) $(plots) $(analyses) effort.tex $(figures) $(trajectories) trajectories_table.tex
manytargets := $(targets) $(logfiles:_log.txt=.xmf) $(logfiles:_log.txt=_filaments.xmf) $(logfiles:_log.txt=_trajectories.xmf) $(phases) $(rhos) $(sigmas)
tramovies := $(logfiles:log.txt=tra_movie.mp4)

main: $(targets)
all: $(manytargets)
movies: $(movies)
figures: $(figures)
tramovies: $(tramovies)
phases: $(phases)
.SECONDARY:

_op=$(wildcard optical*.prep.npz)
optical:
	for f in $(_op:.prep.npz=); do \
		python3 optical_to_simdata.py $${f}.prep.npz $${f}.mask.npz $${f} ;\
		mv $${f}_log.{yaml,txt} ;\
	done

%.npy: %.var
	ithildin-tools convert $< $@ 2>&1

%.xmf: %_log.txt
	ithildin-tools xdmf $(<:_log.txt=) $@

%_filaments.xmf: %_log.txt
	ithildin-tools xdmf -f $(<:_log.txt=) $@

%_trajectories.xmf: %_log.txt
	ithildin-tools xdmf -f $(<:_log.txt=) $@

effort.tex: effort_table.py effort.yaml
	python3 $^ > $@

effort.yaml: phasealpa_7_u.npy effort.py
	python3 effort.py > $@

%.crop.png: %.png
	convert $< -trim +repage -bordercolor white -border 10x10 +repage $@

figure_u.png: figure_u.py table.csv phasealpa_7_u.npy phase_68_u.npy pdl2D_10_u.npy optical_20200204114234_u.npy
	python3 $^ $@

figure_defects_%.png: figure_defects.py %_u.npy %_sigma2glat.npy %_sigma3glat.npy %_rhotopo.npy %_phiactsigma2cos.npy %_phiactsigma3cos.npy %_phiskewsigma2cos.npy %_phiskewsigma3cos.npy %_phiarrsigma2cos.npy %_phiarrsigma3cos.npy %_phiactsigma2cpg.npy %_phiactsigma3cpg.npy %_phiskewsigma2cpg.npy %_phiskewsigma3cpg.npy %_phiarrsigma2cpg.npy %_phiarrsigma3cpg.npy %_phiactrhopc.npy %_phiskewrhopc.npy %_phiarrrhopc.npy %_phiactsigma2pc.npy %_phiactsigma3pc.npy %_phiskewsigma2pc.npy %_phiskewsigma3pc.npy %_phiarrsigma2pc.npy %_phiarrsigma3pc.npy %_phiactrhodipole.npy %_phiskewrhodipole.npy %_phiarrrhodipole.npy %_phiactsigma2ip.npy %_phiactsigma3ip.npy %_phiskewsigma2ip.npy %_phiskewsigma3ip.npy %_phiarrsigma2ip.npy %_phiarrsigma3ip.npy %_phiactrhosvf.npy %_phiskewrhosvf.npy %_phiarrrhosvf.npy
	python3 $< $@ $(wordlist 2,$(words $^),$^)

figure_phases.png: %.png: %.py phasealpa_7_phiact.npy phasealpa_7_phiskew.npy phasealpa_7_phiarr.npy phase_68_phiact.npy phase_68_phiskew.npy phase_68_phiarr.npy pdl2D_10_phiact.npy pdl2D_10_phiskew.npy pdl2D_10_phiarr.npy optical_20200204114234_phiact.npy optical_20200204114234_phiskew.npy optical_20200204114234_phiarr.npy
	python3 $< $@ $(wordlist 2,$(words $^),$^)

figure_phases_corr.png: %.png: %.py phase_68_phiact.npy phase_68_phiskew.npy phase_68_phiarr.npy
	python3 $< $@ $(wordlist 2,$(words $^),$^)

figure_process.png: %.png: %.py phase_68_u.npy phase_68_phiarr.npy phase_68_phiarrrhopc.npy
	python3 $< $@ $(wordlist 2,$(words $^),$^)

figure_characteristic_%.png: figure_characteristic.py %.npy
	python3 $^ $@

figure_sims.png: %.png: %.py pdl2D_10_phiarrrhopc.npy pdl2D_10_phiskewrhopc.npy pdl2D_10_phiactrhopc.npy phasealpa_7_phiarrrhopc.npy phasealpa_7_phiskewrhopc.npy phasealpa_7_phiactrhopc.npy phase_68_phiarrrhopc.npy phase_68_phiskewrhopc.npy phase_68_phiactrhopc.npy
	python3 $< $@ $(wordlist 2,$(words $^),$^)

$(plots): %.png: %.npy
	ithildin-tools plot $(shell echo $< | cut -d_ -f1,2) \
		$(shell echo $< | cut -d_ -f3 | sed 's/\.[^.]*$$//') \
		$(shell echo $< | cut -d_ -f3 | sed -n '/sigma/{s/^.*$$/-cPurples/;p};/rho/{s/^.*$$/-cGreens/;p};/^phi/{s/^.*$$/-ctwilight/;p};') \
		--dpi=180 --it=-1 -o $@

%.mp4: %.npy
	ithildin-tools movie --dpi=180 $(shell echo $< | cut -d_ -f1,2) \
		$(shell echo $< | cut -d_ -f3 | sed 's/\.[^.]*$$//') \
		$(shell echo $< | cut -d_ -f3 | sed -n '/sigma/{s/^.*$$/-cPurples/;p};/rho/{s/^.*$$/-cGreens --vmin 0/;p};/^phi/{s/^.*$$/-ctwilight --vmin 0 --vmax 6.283185307179586/;p};/^u.npy$$/{s/^.*$$/--vmin '-0.25' --vmax 1.25/;p}') \
		-o $@

$(stateplots): %_statespace.png: %_u.npy table.csv
	python3 statespace.py $(shell echo $< | cut -d_ -f1,2) table.csv $@

$(apdplots): %_apd.png: %_u.npy
	python3 calcapd.py $< $@

%_phiact.npy: %_u.npy %_log.txt %_v.npy table.csv
	ithildin-tools phase -a act \
		-k $$(python3 tablestem.py $(word 2,$^) table.csv u=V v=R u_iso=Vstar v_iso=Rstar | sed "s/'/\\\"/g;s/ //g") \
		$(shell echo $< | cut -d_ -f1,2) $@
%_phiarr.npy: %_u.npy %_log.txt table.csv
	ithildin-tools phase -a arr \
		-k $$(python3 tablestem.py $(word 2,$^) table.csv u_iso dur | sed "s/'/\\\"/g;s/ //g") \
		$(shell echo $< | cut -d_ -f1,2) $@
%_phiskew.npy: %_u.npy %_log.txt %_v.npy table.csv
	ithildin-tools phase -a skew \
		-k $$(python3 tablestem.py $(word 2,$^) table.csv u=V v=R u_iso=Vstar v_iso=Rstar p0 p1 | sed "s/'/\\\"/g;s/ //g") \
		$(shell echo $< | cut -d_ -f1,2) $@

%_rhoglat.npy: %_u.npy %_log.txt table.csv
	ithildin-tools phase-defect -a glat \
		-k $$(python3 tablestem.py $(word 2,$^) table.csv u=V u_iso=Vstar | sed "s/'/\\\"/g;s/ //g") \
		$(shell echo $< | cut -d_ -f1,2) $@

%_rhotopo.npy: %_u.npy %_log.txt table.csv
	ithildin-tools phase-defect -a topo \
		-k $$(python3 tablestem.py $(word 2,$^) table.csv u=V v=R u_iso=Vstar v_iso=Rstar | sed "s/'/\\\"/g;s/ //g") \
		$(shell echo $< | cut -d_ -f1,2) $@

%rhorpg.npy: %.npy
	ithildin-tools phase-defect -a rpg \
		-p $(shell echo $@ | cut -d_ -f3 | sed 's/rho.*$$//') \
		$(shell echo $< | cut -d_ -f1,2) $@

%rhocpg.npy: %.npy
	ithildin-tools phase-defect -a cpg \
		-p $(shell echo $@ | cut -d_ -f3 | sed 's/rho.*$$//') \
		$(shell echo $< | cut -d_ -f1,2) $@

%rhodipole.npy: %.npy
	ithildin-tools phase-defect -a dipole \
		-p $(shell echo $@ | cut -d_ -f3 | sed 's/rho.*$$//') \
		$(shell echo $< | cut -d_ -f1,2) $@

%rhomph.npy: %.npy
	ithildin-tools phase-defect -a mph \
		-p $(shell echo $@ | cut -d_ -f3 | sed 's/rho.*$$//') \
		$(shell echo $< | cut -d_ -f1,2) $@

%rhopc.npy: %.npy
	ithildin-tools phase-defect -a pc \
		-p $(shell echo $@ | cut -d_ -f3 | sed 's/rho.*$$//') \
		$(shell echo $< | cut -d_ -f1,2) $@

%rhocos.npy: %.npy
	ithildin-tools phase-defect -a cos \
		-p $(shell echo $@ | cut -d_ -f3 | sed 's/rho.*$$//') \
		$(shell echo $< | cut -d_ -f1,2) $@

%rhosvf.npy: %.npy
	ithildin-tools phase-defect -a svf \
		-p $(shell echo $@ | cut -d_ -f3 | sed 's/rho.*$$//') \
		$(shell echo $< | cut -d_ -f1,2) $@

%rhoip.npy: %.npy
	ithildin-tools phase-defect -a ip \
		-p $(shell echo $@ | cut -d_ -f3 | sed 's/rho.*$$//') \
		$(shell echo $< | cut -d_ -f1,2) $@

%_sigma2glat.npy: %_u.npy %_log.txt
	ithildin-tools phase-defect -a glat -e 2\
		$(shell echo $< | cut -d_ -f1,2) $@

%_sigma3glat.npy: %_u.npy %_log.txt
	ithildin-tools phase-defect -a glat -e 3\
		$(shell echo $< | cut -d_ -f1,2) $@

%sigma2rpg.npy: %.npy
	ithildin-tools phase-defect -a rpg -e 2 \
		-p $(shell echo $@ | cut -d_ -f3 | sed 's/sigma.*$$//') \
		$(shell echo $< | cut -d_ -f1,2) $@

%sigma2cpg.npy: %.npy
	ithildin-tools phase-defect -a cpg -e 2 \
		-p $(shell echo $@ | cut -d_ -f3 | sed 's/sigma.*$$//') \
		$(shell echo $< | cut -d_ -f1,2) $@

%sigma2ip.npy: %.npy
	ithildin-tools phase-defect -a ip -e 2 \
		-p $(shell echo $@ | cut -d_ -f3 | sed 's/sigma.*$$//') \
		$(shell echo $< | cut -d_ -f1,2) $@

%sigma2pc.npy: %.npy
	ithildin-tools phase-defect -a pc -e 2 \
		-p $(shell echo $@ | cut -d_ -f3 | sed 's/sigma.*$$//') \
		$(shell echo $< | cut -d_ -f1,2) $@

%sigma2cos.npy: %.npy
	ithildin-tools phase-defect -a cos -e 2 \
		-p $(shell echo $@ | cut -d_ -f3 | sed 's/sigma.*$$//') \
		$(shell echo $< | cut -d_ -f1,2) $@

%sigma3rpg.npy: %.npy
	ithildin-tools phase-defect -a rpg -e 3 \
		-p $(shell echo $@ | cut -d_ -f3 | sed 's/sigma.*$$//') \
		$(shell echo $< | cut -d_ -f1,2) $@

%sigma3cpg.npy: %.npy
	ithildin-tools phase-defect -a cpg -e 3 \
		-p $(shell echo $@ | cut -d_ -f3 | sed 's/sigma.*$$//') \
		$(shell echo $< | cut -d_ -f1,2) $@

%sigma3ip.npy: %.npy
	ithildin-tools phase-defect -a ip -e 3 \
		-p $(shell echo $@ | cut -d_ -f3 | sed 's/sigma.*$$//') \
		$(shell echo $< | cut -d_ -f1,2) $@

%sigma3pc.npy: %.npy
	ithildin-tools phase-defect -a pc -e 3 \
		-p $(shell echo $@ | cut -d_ -f3 | sed 's/sigma.*$$//') \
		$(shell echo $< | cut -d_ -f1,2) $@

%sigma3cos.npy: %.npy
	ithildin-tools phase-defect -a cos -e 3 \
		-p $(shell echo $@ | cut -d_ -f3 | sed 's/sigma.*$$//') \
		$(shell echo $< | cut -d_ -f1,2) $@

trajectories_angle.pdf: trajectories_angle.py phase_68_tra.txt phase_68_tra_idx.txt optical_20200204114234_tra.txt optical_20200204114234_tra_idx.txt
	python3 $^ $@

trajectories_length.pdf: trajectories_length.py phase_68_tra.txt phase_68_tra_idx.txt optical_20200204114234_tra.txt optical_20200204114234_tra_idx.txt trajectories_angle.py
	python3 $^ $@

trajectories_table.tex: trajectories_table.py $(logfiles:log.txt=tra.txt) $(logfiles:log.txt=tra_idx.txt) trajectories_angle.py
	python3 $^ $@

%_tra_movie.mp4: trajectories_movie.py %_tra.txt %_phiarr.npy
	python3 $^ $@

%_tra.txt: trajectories_find.py %_phiarrrhopc.npy %_phiarr.npy
	python3 $^ $@

%_tra_idx.txt:
	echo 'Choosing trajectory with index 0 by default for analysis.'
	echo 0 > $@

# lowresstem = phase_68
# lowressel = lowres_8_log.txt lowres_32_log.txt lowres_64_log.txt
lowresstem = pdl2D_10
lowressel = lowres_16_log.txt lowres_64_log.txt lowres_128_log.txt
lowres = lowres_1_log.txt $(lowressel)
$(lowres): lowres_%_log.txt: $(lowresstem)_log.txt
	python3 lowreslog.py $< $(shell echo $@ | cut -d_ -f2) $@
$(lowres:_log.txt=_u.npy): lowres_%_u.npy: $(lowresstem)_u.npy
	python3 lowresnpy.py $< $(shell echo $@ | cut -d_ -f2) $@

lowrespng = lowres_phiarrrhorpg.png lowres_phiarrrhocpg.png lowres_phiarrrhodipole.png lowres_phiarrrhomph.png lowres_phiarrrhocos.png lowres_phiarrrhopc.png lowres_phiarrrhosvf.png lowres_phiarrrhoip.png lowres_rhoglat.png
lowres: $(lowrespng)
$(lowrespng): lowres_%.png: $(lowres:_log.txt=_%.npy)
	python3 lowrespng.py $^ $@

lowresrho = phiarrrhopc
# lowresrho = phiarrrhocpg
# lowresrho = rhoglat
figure_lowres.png: figure_lowres.py $(lowressel:log.txt=u.npy) $(lowressel:log.txt=$(lowresrho).npy)
	python3 $< $@ $(wordlist 2,$(words $^),$^)

forcePDLstem = forcePDL_62
forcePDLlowresstem = forcePDLlowres_62
forcePDLlowresstep = 8
$(forcePDLlowresstem:=_log.txt): $(forcePDLstem:=_log.txt)
	python3 lowreslog.py $< $(forcePDLlowresstep) $@
$(forcePDLlowresstem:=_u.npy): $(forcePDLstem:=_u.npy) $(forcePDLstem:=_inhom.npy)
	python3 lowresnpy.py $^ $(forcePDLlowresstep) $@
$(forcePDLlowresstem:=_v.npy): $(forcePDLstem:=_v.npy) $(forcePDLstem:=_inhom.npy)
	python3 lowresnpy.py $^ $(forcePDLlowresstep) $@
forcePDLlowres: $(forcePDLlowresstem:=_log.txt) $(forcePDLlowresstem:=_u.npy) $(forcePDLlowresstem:=_phiarr.npy) $(forcePDLlowresstem:=_phiarrrhopc.npy) $(forcePDLlowresstem:=_u.png) $(forcePDLlowresstem:=_phiarr.png) $(forcePDLlowresstem:=_phiarrrhopc.png)

figure_groundtruth_$(forcePDLlowresstem).png: figure_groundtruth_%.png: figure_groundtruth.py $(forcePDLstem:=_inhom.npy) %_rhoglat.npy %_rhotopo.npy %_phiactrhocos.npy %_phiskewrhocos.npy %_phiarrrhocos.npy %_phiactrhocpg.npy %_phiskewrhocpg.npy %_phiarrrhocpg.npy %_phiactrhopc.npy %_phiskewrhopc.npy %_phiarrrhopc.npy %_phiactrhodipole.npy %_phiskewrhodipole.npy %_phiarrrhodipole.npy %_phiactrhoip.npy %_phiskewrhoip.npy %_phiarrrhoip.npy %_phiactrhosvf.npy %_phiskewrhosvf.npy %_phiarrrhosvf.npy
	python3 $^ $@

noisestem = phase_68
# noisestem = pdl2D_10
noise = noise_00_log.txt noise_05_log.txt noise_10_log.txt noise_15_log.txt noise_20_log.txt noise_1000_log.txt
$(noise): noise_%_log.txt: $(noisestem)_log.txt noise_%_u.npy noiselog.py
	python3 noiselog.py $< $(shell echo $@ | cut -d_ -f2) $@
$(noise:_log.txt=_u.npy): noise_%_u.npy: $(noisestem)_u.npy noisenpy.py
	python3 noisenpy.py $< $(shell echo $@ | cut -d_ -f2) $@

# noisepng = noise_phiarrrhorpg.png noise_phiarrrhocpg.png noise_phiarrrhodipole.png noise_phiarrrhomph.png noise_phiarrrhocos.png noise_phiarrrhopc.png noise_phiarrrhosvf.png noise_phiarrrhoip.png noise_rhoglat.png
noiserho = phiarrrhopc
noisepng = noise_$(noiserho).png
noiseanalyse: $(noisepng)
noise: $(noise:log.txt=$(noiserho).png) $(noise:log.txt=u.png)
$(noisepng): noise_%.png: noisepng.py $(noise:_log.txt=_%.npy)
	python3 $^ $@

figure_noise.png: figure_noise.py noise_15_u.npy noise_10_u.npy noise_05_u.npy noise_15_$(noiserho).npy noise_10_$(noiserho).npy noise_05_$(noiserho).npy
	python3 $< $@ $(wordlist 2,$(words $^),$^)

clean:
	rm -rf $(manytargets) $(optical:.prep.npz=*.{npy,txt}) *.png.tex figure_*.{0,1,2}.png *.npy *.mp4 noise_*_log.txt lowres_*_log.txt

include initdata.d

.PHONY: all clean
