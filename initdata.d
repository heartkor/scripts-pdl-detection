# Download initial data from Zenodo or KU Leuven iRODS.
# Uncomment the downloader you want to use!

MD5FILE = initdata.md5
FILES = $(shell sed 's/^.*\t//' ${MD5FILE})

initdata-verify: ${MD5FILE}
	md5sum -c $<

initdata-remove:
	${RM} ${FILES}

initdata-download: ${FILES}

ZENODOURLPREFIX = https://zenodo.org/record/6477532/files/
ZENODOURLSUFFIX = ?download=1
${FILES}:
	wget ${ZENODOURLPREFIX}$@${ZENODOURLSUFFIX} -O $@

# IRODSCOLLECTION = /set/home/set_pilot030/Simulations/ithildin/
# ${FILES}:
# 	minirods -v download ${IRODSCOLLECTION}$(shell echo $@ | sed 's/^\([^_]\+_[0-9]\+\).*$$/\1/')/$@ $@
