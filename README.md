# Evaluate data for phase defect detection algorithm paper

Analyse results from `ithildin` using `py_ithildin`.
Run `make` to compile all the figures and to collect data for tables.
This requires quite a lot of time and memory.

<!-- ls -l | grep -- '..\/fig' | cut -d ' ' -f 11,13 | sed 's/\.\.\/figures\///g;s/^Fig\([0-9]\+\)\.\w\+/\1\./' | sort -n -->

Figures in the paper:

1. figure_u.png
2. figure_process.png
3. figure_phases.png
4. figure_phases_corr.png
5. figure_edge_vertex.py.pdf
6. figure_defects_phase_68.0.png
7. figure_defects_phase_68.1.png
8. figure_defects_corr_phase_68_phiarr.png
9. figure_defects_corr_phase_68.png
10. figure_defects_optical_20200204114234.0.png
11. figure_defects_optical_20200204114234.1.png
12. figure_defects_corr_optical_20200204114234_phiarr.png
13. trajectories_length.pdf
14. trajectories_angle.pdf

Supplementary figures:

1. figure_defects_phasealpa_7.0.png
2. figure_defects_phasealpa_7.1.png
3. figure_defects_pdl2D_10.0.png
4. figure_defects_pdl2D_10.1.png

Please cite this paper when using the implementation:

Kabus D, Arno L, Leenknegt L, Panfilov AV, Dierckx H (2022) Numerical methods
for the detection of phase defect structures in excitable media. PLOS ONE
17(7): e0271351. https://doi.org/10.1371/journal.pone.0271351
