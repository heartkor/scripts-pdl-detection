#!/bin/env python3
"""
A figure showing the characteristic function for the last half of a recording
"""
from figure_helper import *
from figure_groundtruth import approximate_characteristic_function
import numpy as np
import matplotlib.pyplot as plt
import sys

sd, rho, varname = load_simdata_from_filename(sys.argv[1])
outfile = sys.argv[-1]

fig, axes = subplots(1, 1, figsize=(8*cm, 7*cm), sharex=True, sharey=True)
ax = axes[0]

idx = range(int(0.5*sd.shape[0]), int(0.75*sd.shape[0]))

chi = approximate_characteristic_function(rho[idx])[0]

x, y = sd.meshgrid(-1, -2)

qm = ax.pcolormesh(x, y, np.where(sd.mask[0], chi, np.nan), shading="nearest", cmap="Oranges")
cbar = fig.colorbar(qm, ax=ax)

cbar.set_ticks([0,1])
cbar.set_ticklabels([0,1])

plt.tight_layout()
plt.savefig(outfile)
