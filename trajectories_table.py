#!/bin/env python3
"""
Plot the length of a PDL over time
"""
import ithildin as ith
import numpy as np
import re
import sys
from trajectories_angle import read_pdl_trajectory
from figure_helper import load_simdata_from_filename

def str_value_err(value, error, digits=2):
    digits = digits - 1 - int(np.floor(np.log10(error)))
    return f"{value:.{max([digits,0])}f}({error*10**digits:.0f})"

def str_value_err_exp(value, error):
    exponent = int(np.floor(np.log10(value)))
    return str_value_err(value/10**exponent, error/10**exponent) + f"e{exponent}"

with open(sys.argv[-1], "w") as out:
    out.write(" & ".join(f"\\textbf{{{s}}}" for s in ["model / experiment", "average length $L$", "precession period $T$"])+"\\\\\n")
    out.write("\\hline\n")

    filenames = sys.argv[1:-1]
    N = len(filenames)//2
    for filename_tra, filename_idx in zip(filenames[:N], filenames[N:]):
        sd, _, _ = load_simdata_from_filename(filename_tra)
        use_units = not "Aliev-Panfilov" in sd.log.modeltype
        times, betas, lengths, ells = read_pdl_trajectory(filename_tra, filename_idx)

        length_mean = np.mean(lengths)
        length_std = np.std(lengths)

        params, cov = np.polyfit(times, betas, deg=1, cov=True)
        beta_fit = np.poly1d(params)
        omega_mean = params[0]
        omega_std = cov[0][0]**0.5
        period_mean = 2*np.pi/np.abs(omega_mean)
        period_std = 2*np.pi/omega_mean**2*omega_std

        label = {
                "optical mapping data": "hiAM optical mapping",
                "Bueno-Orovio 2008, 4 var": "BOCF simulation",
                "Fenton-Karma 3 var MLRI": "FK simulation",
                "Aliev-Panfilov, continuous epsilon": "AP simulation",
        }

        unitx = '\\milli\\metre' if use_units else ''
        unitt = '\\milli\\second' if use_units else ''
        out.write(f"{label[sd.log.modeltype]} & \\SI{{{str_value_err(length_mean, length_std)}}}{{{unitx}}} & \\SI{{{str_value_err(period_mean, period_std)}}}{{{unitt}}} \\\\\n")

    out.write("\\hline\n")
