#!/bin/env python3
"""
Plot the angle of a PDL over time
"""
import ithildin as ith
import numpy as np
import matplotlib.pyplot as plt
import sys
from figure_helper import *
from matplotlib import cm

PDL = ith.PDL
with open(sys.argv[1], "r") as f:
    trajectories = eval(f.read())

sd, _, varname = load_simdata_from_filename(sys.argv[2])
dt, dz, dy, dx = sd.deltas
Nt, Nz, Ny, Nx = sd.shape
ot, oz, oy, ox = sd.origin
T,  Lz, Ly, Lx = sd.physical_size
t = sd.linspace(0)
it_ = max([Nt//2, Nt-300])

pdls_per_frame = dict()
for idx_traj, trajectory in enumerate(trajectories):
    if len(trajectory) < 3:
        continue
    for pdl in trajectory:
        pdl.idx_traj = idx_traj
        it = pdl.it = min([Nt-1, int(pdl.time/dt + 0.2)])
        if it not in pdls_per_frame:
            pdls_per_frame[it] = []
        pdls_per_frame[it].append(pdl)

cmap_traj = cm.get_cmap("tab10")
N_traj = len(trajectories)

def plot_pdl_trajectories(it, ax):
    ret = []
    # cb, qm = sd.plot(varname, it=it, ax=ax, cmap="twilight", vmin=0, vmax=2*np.pi)
    # cb.remove()
    # ret.append(qm)
    if it in pdls_per_frame:
        for pdl in pdls_per_frame[it]:
            if len(pdl) < 5:
                continue
            c = cmap_traj((pdl.idx_traj % 10)/10)
            ret.extend(pdl.plot(color=c, ax=ax))
            ret.append(ax.text(*np.mean(pdl.points, axis=0), f"{pdl.idx_traj}", ha="right", va="center"))
    ax.set_xlim([ox, ox + Lx])
    ax.set_ylim([oy, oy + Ly])
    ax.set_title(f"$t$ = {t[it]} ms")
    return ret

fig = plt.figure(figsize=(6,6), dpi=200)
ax = fig.add_subplot()
ith.plot.movie(fig, ax, plot_pdl_trajectories, [[it] for it in np.arange(it_, Nt)], filename=sys.argv[3])
