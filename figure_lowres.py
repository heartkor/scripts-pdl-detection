#!/bin/env python3
"""
A figure showing steps to get to PDLs.
"""
from figure_helper import *
import numpy as np
import matplotlib.pyplot as plt
import sys

outfile = sys.argv[1]
data = [load_simdata_from_filename(a) for a in sys.argv[2:]]

rhomin = 0
# rhomax = max(np.nanmax(u) for sd, u, varname in data[3:])
rhomax = 1
# rhomax = None

Nrow = len(data)//3
fig, axes = subplots(Nrow, 3, figsize=(maxwidthin, Nrow*6*cm), sharey="all", sharex="all")

for ((i, ax), (sd, u, varname)) in zip(enumerate(axes), data):
    print(f"{outfile}: {varname}")
    x, y = sd.meshgrid(-1, -2)
    ufr = np.where(sd.mask[0], u[-20,0], np.nan)

    if varname == "u":
        qm = ax.pcolormesh(x, y, ufr, shading="nearest", cmap="inferno", vmin=UMIN, vmax=UMAX)
    else:
        qm = ax.pcolormesh(x, y, ufr, shading="nearest", cmap="Greens", vmin=rhomin, vmax=rhomax)

    res = '\\times'.join(str(s) for s in sd.shape[-2:])
    ax.set_title(f"({sublabel(i)}) ${label2formula(varname)}$ at ${res}$")
    ax.set_facecolor("silver")

Oy, Ox = 0, 0
_, _, Ly, Lx = sd.log.physical_size
xlims = [Ox, Ox+Lx]
ylims = [Oy, Oy+Ly]
for ax in axes:
    ax.set_xlim(xlims)
    ax.set_ylim(ylims)

plt.tight_layout()

add_colorbar_u(fig, axes[:3])
add_colorbar(fig, axes[3:], ticks=[rhomin, rhomax], cmap="Greens")

plt.savefig(outfile)
