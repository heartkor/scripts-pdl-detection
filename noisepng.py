#!/bin/env python3
'''
Plot similarity of lower resolution phase defect fields rho
'''
import numpy as np
import matplotlib.pyplot as plt
import ithildin as ith
import sys

if __name__ == '__main__':
    sds = dict()
    varname = None
    for varfilename in sys.argv[1:-1]:
        series, number, varname = varfilename.split('_')
        varname = varname.split('.')[0]
        sd = ith.SimData.from_stem(f'{series}_{number}')
        sds[sd.log["SNRdB"]] = sd
    snr_min = min(sds.keys())
    snr_max = max(sds.keys())

    '''
    fig, (ax0, ax1) = plt.subplots(1, 2, figsize=(12, 5))

    # vmax = max(np.nasnr_max(sd.vars[varname][-1]) for sd in sds.values())
    vmax = None

    # interpolation = 'bilinear'
    interpolation = 'none'

    sds[snr_min].plot(varname, it=-1, ax=ax0, interpolation=interpolation, cmap='Greens', vmin=0, vmax=vmax)
    sds[snr_max].plot(varname, it=-1, ax=ax1, interpolation=interpolation, cmap='Greens', vmin=0, vmax=vmax)

    for ax in [ax0, ax1]:
        ax.set_aspect(1)
        ax.set_xlim(100, 180)
        ax.set_ylim(175, 255)
    '''

    true = sds[snr_max].vars[varname]
    snrs = sorted(snr for snr in sds.keys() if snr != snr_max)
    preds = [np.nanmean((true - sds[snr].vars[varname])**2)**.5 for snr in snrs]

    plt.plot(snrs, preds)

    plt.xlabel("SNR in dB")
    plt.ylabel("deviation from true case")

    plt.tight_layout()
    plt.savefig(sys.argv[-1])
    # fig.savefig(sys.argv[-1])
