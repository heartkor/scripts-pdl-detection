#!/bin/env python3
'''
Add noise to a varfile
'''
import ithildin as ith
import sys
import numpy as np

if __name__ == '__main__':
    v = ith.varfile.read_var(sys.argv[1])
    SNRdB = int(sys.argv[2])
    SNR = 10**(0.1*SNRdB)

    std_signal = np.nanstd(v)
    std_noise = std_signal/SNR

    v_noisy = v + np.random.normal(size=v.shape, scale=std_noise)

    np.save(sys.argv[3], v_noisy)
