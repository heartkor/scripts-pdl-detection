#!/bin/env python3
"""
A figure showing three frames of u and the tip trajectory for three
simulations
"""
from figure_helper import *
import numpy as np
import matplotlib.pyplot as plt
import sys
from tablestem import *

outfile = sys.argv[-1]
tablefile = sys.argv[1]

Nrows = len(sys.argv[2:-1])
fig, allaxes = subplots(Nrows, 4, figsize=(maxwidthin, Nrows*60*mm), sharey="row", constrained_layout=True)
allaxes.shape = (-1, 4)
vmin, vleft, vright, vmax = UMIN, 0., 1., UMAX

for modelfilename, (iax, axes) in zip(sys.argv[2:-1], enumerate(allaxes)):
    sd, u, varname = load_simdata_from_filename(modelfilename)
    Nt, Nz, Ny, Nx = sd.shape
    dt, dz, dy, dx = sd.deltas
    x, y = sd.meshgrid(-1, -2)
    times = sd.times

    apd = read_parameters_for_model(tablefile, sd.log.modeltype)["dur"]
    sel_frames = [-int(0.5*apd/sd.deltas[0]), -int(0.25*apd/sd.deltas[0]), -1]

    for (jax, ax), it in zip(enumerate(axes), sel_frames):
        print(f"axis {iax},{jax}")
        t = times[it]
        ufr = np.where(sd.mask[0], u[it,0], np.nan)
        # vmin, vmax = np.nanmin(ufr), np.nanmax(ufr)
        qm = ax.pcolormesh(x, y, ufr, shading="nearest", cmap="inferno", vmin=vmin, vmax=vmax)
        ax.set_title(f"({sublabel(iax, jax, 4)}) {modelabbr[sd.log.modeltype]}: $t={int(t)}\\mathrm{{ms}}$")

    print(f"axis {iax},-1")
    jax += 1
    ax = axes[-1]
    ax.set_xlim(x[0,0], x[-1,-1])
    ax.set_ylim(y[0,0], y[-1,-1])
    ax.set_title(f"({sublabel(iax, jax, 4)}) {modelabbr[sd.log.modeltype]}: trajectory of PS")

    t_end = sd.times[-1]
    t_start = t_end - 4*apd

    if sd.filaments is None:
        Ix = np.arange(Nx)
        Iy = np.arange(Ny)
        fils = []
        for it in range(np.argmin(np.abs(times - t_start)), Nt):
            mu = (sd.vars["u"][it,0] > 0.5)
            mv = (sd.vars["v"][it,0] > 0.5)
            PSs, _, _ = ith.topology.intersection_of_contours_of_masks(mu, mv, Ix, Iy)
            for PS in PSs:
                if sd.mask[0,round(PS[1]), round(PS[0])]:
                    fils.append(ith.Filament(np.reshape(PS, (1, 2)), res=[dx, dy], time=times[it]))
        sd.filaments = fils

    if "Aliev-Panfilov" in sd.log.modeltype:
        trajectories = [sd.filaments]
    else:
        trajectories = ith.filament.find_trajectories(sd.filaments, maxdist=10)

    for trajectory in trajectories:
        traj = [f for f in trajectory if f.time > t_start]
        times = np.array([f.time for f in traj])
        traj = np.array([f.points[0,:2] for f in traj])
        if traj.ndim == 2 and traj.shape[1] == 2 and traj.shape[0] >= 1:
            ith.plot.colorline(*traj.T, times, ax=ax, cmap="Purples", vmin=t_start, vmax=t_end)
        plt.imshow(sd.mask[0], extent=[x[0,0], x[-1,-1], y[0,0], y[-1,-1]], vmin=-2, vmax=1, cmap="binary_r", origin="lower")

add_colorbar_u(fig, axes, location="bottom")

# plt.tight_layout()
plt.savefig(outfile)
