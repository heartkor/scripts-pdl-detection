#!/bin/env python3
"""
Convert sappho.OpticalData to ithildin.SimData and write to disk
"""
import numpy as np
import matplotlib.pyplot as plt
import sappho
import ithildin as ith
import sys
from typing import Tuple
import yaml

def split_name(filename:str) -> Tuple[str,str,str,str]:
    _ = filename.split(".")
    base, ext = ".".join(_[:-1]), _[-1]
    _ = base.split("_")
    stem, varname = "_".join(_[:-1]), _[-1]
    return base, stem, varname, ext

od = sappho.OpticalData(sys.argv[1])
mask = sappho.OpticalData(sys.argv[2]).frames[0] > 0.5
sd = od.to_ithildin(mask=mask)

duration_one_rotation = 132 # ms
frames_delay = int(0.15*duration_one_rotation/od.dt)
sd.vars["v"] = np.roll(sd.vars["u"], frames_delay, axis=0)
sd.vars["v"][:frames_delay] = 0

sd.to_stem(sys.argv[3])
