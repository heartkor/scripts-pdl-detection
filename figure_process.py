#!/bin/env python3
"""
A figure showing steps to get to PDLs.
"""
from figure_helper import *
import numpy as np
import matplotlib.pyplot as plt
import sys
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes, mark_inset

outfile = sys.argv[1]
data = [load_simdata_from_filename(a) for a in sys.argv[2:]]

# thresh = data[3][1]
# thresh = thresh > 0.1*np.reshape(np.nanmax(thresh, axis=(1, 2, 3)), (-1, 1, 1, 1))
# data[3] = (data[3][0], thresh)

fig, axes = subplots(1, 3, figsize=(maxwidthin, 6*cm), sharex=True, sharey=True)

cmaps = ["inferno", "twilight", "Greens"]
vlims = [(UMIN, UMAX), (0, 2*np.pi), (0, 1)]
textcolors = ["w", "k", "k"]

cbars = []
for ((i, ax), (sd, u, varname), cmap, (vmin, vmax), textcolor) in zip(enumerate(axes), data, cmaps, vlims, textcolors):
    print(f"{outfile}: {varname}")
    x, y = sd.meshgrid(-1, -2)
    ufr = u[-20,0]

    if sd.log.stem in zoomlims:
        lims = zoomlims[sd.log.stem]
        locs = zoomlocs[sd.log.stem]
        axins = zoomed_inset_axes(ax, 5, loc=locs[0])
        axins.set_xlim(lims[0])
        axins.set_ylim(lims[1])
        axins.set_xticks([])
        axins.set_yticks([])
        mark_inset(ax, axins, loc1=locs[1], loc2=locs[2], fc="none", ec=(0.5, 0.5, 0.5, 1.), lw=0.5)

        subaxes = [ax, axins]
    else:
        subaxes = [ax]

    for a in reversed(subaxes):
        qm = a.pcolormesh(x, y, np.where(sd.mask[0], ufr, np.nan), shading="nearest", cmap=cmap, vmin=vmin, vmax=vmax)

    # ax.text(0.05, 0.95, f"({sublabel(i)}) ${label2formula(varname)}$", c=textcolor, horizontalalignment="left", verticalalignment="top", transform=ax.transAxes)
    ax.set_title(f"({sublabel(i)}) ${label2formula(varname)}$")
    cbars.append(fig.colorbar(qm, ax=ax))

cbars[0].set_ticks([0,1])
cbars[0].set_ticklabels([0,1])

cbars[1].set_ticks([0, np.pi, 2*np.pi])
cbars[1].set_ticklabels(["0", "$\\pi$", "$2\\pi$"])

cbars[2].set_ticks(vlims[2])
cbars[2].set_ticklabels(vlims[2])

plt.tight_layout()
plt.savefig(outfile)
