#!/bin/env python3
"""
A figure showing steps to get to PDLs.
"""
from figure_helper import *
import numpy as np
import matplotlib.pyplot as plt
import sys
import re

outfile = sys.argv[1]
data = [load_simdata_from_filename(a) for a in sys.argv[2:]]

Nrow = 3
fig, axes = subplots(Nrow, 3, figsize=(maxwidthin, min([Nrow*6*cm, maxheightin])), sharey="row")

zoomlims = dict(
    pdl2D_10=((135,190),25),
    phasealpa_7=((50,30),20),
    phase_68=((70,60),15),
)
zoomlims = {k: ((x-r,x+r),(y-r,y+r)) for k, ((x,y), r) in zoomlims.items()}
zoomlims = [zoomlims[re.findall(r"^\w+_[0-9]+", a)[0]] for a in sys.argv[2:]]

cbars = []
for ((i, ax), (sd, u, varname), (xlim, ylim)) in zip(enumerate(axes), data, zoomlims):
    print(f"{outfile}: {varname}")
    for a in [ax.xaxis, ax.yaxis]:
        a.set_major_locator(ticker.MaxNLocator(2))
        a.set_minor_locator(ticker.MaxNLocator(6))
    x, y = sd.meshgrid(-1, -2)
    qm = ax.pcolormesh(x, y, np.where(sd.mask[0], u[-1,0], np.nan), cmap="Greens", shading="nearest")
    ax.text(0.05, 0.95, f"({sublabel(i)}) {modelabbr[sd.log.modeltype]} ${label2formula(varname)}$", horizontalalignment="left", verticalalignment="top", transform=ax.transAxes)
    ax.set_xlim(*xlim)
    ax.set_ylim(*ylim)
    if "Aliev-Panfilov" in sd.log.modeltype:
        ax.set_xlabel("$x$")
        if i == 3:
            ax.set_ylabel("$y$")

plt.tight_layout()
plt.savefig(outfile)
