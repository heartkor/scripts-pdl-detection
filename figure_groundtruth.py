#!/bin/env python3
"""
For a simulation where a long obstacle is placed at high resolution that the
spiral snaps to, we know that the long obstacle will become the PDL. So we
know the ground truth in this case. To then make the obstacle "invisible", we
look at a worse resolution, where the obstacle's width is less than a pixel
width.

This script takes as input the inhom file of the simulation at high
resolution. Finally all other varfiles in the inputs are rho's at low
resolution, i.e. phase defect fields centered on vertices.

For each of those, this script calculates the overlap with the obstacle. We
know which pixels at the bad resolution should contain the PDL from the
high-resolution inhom file.

The mean-squared error for each of those rho files will be output.
"""
import numpy as np
import matplotlib.pyplot as plt
import ithildin as ith
import sys
from figure_helper import *
from itertools import chain
from lowresnpy import reduce

cmap = plt.get_cmap("viridis", 4)
# cmap = plt.get_cmap("RdYlGn", 4)
# cmap = plt.get_cmap("plasma", 4)
# cmap = plt.get_cmap("cividis", 4)

# cmap = plt.get_cmap("viridis")
# cmap = plt.get_cmap("Oranges")

def approximate_characteristic_function(rho:np.ndarray) -> np.ndarray:
    # rho_mean = np.nanmax(rho, axis=0)
    rho_mean = np.nanmean(rho, axis=0)

    # vmin = np.nanmin(rho_mean)
    # vmin = np.nanpercentile(rho_mean, 90)
    vmin = np.nanpercentile(rho_mean, 40)

    # vmax = np.nanmax(rho_mean)
    # vmax = np.nanpercentile(rho_mean, 99)
    vmax = np.nanpercentile(rho_mean, 99)

    chi = (rho_mean - vmin) / (vmax - vmin)
    return np.clip(chi, 0, 1)

def score(rho:np.ndarray, obstacle:np.ndarray) -> tuple[int,int,int,int,np.ndarray]:
    pred = approximate_characteristic_function(rho) > 0.5
    true = obstacle[0]

    valid = np.max(np.isfinite(rho), axis=(0,1))
    TP = np.sum(np.where(valid,  pred &  true, 0))
    FP = np.sum(np.where(valid,  pred & ~true, 0))
    FN = np.sum(np.where(valid, ~pred &  true, 0))
    TN = np.sum(np.where(valid, ~pred & ~true, 0))

    colouring = np.where(valid, true[0] + 2* pred[0], np.nan)

    return TP, FP, FN, TN, colouring

if __name__ == "__main__":

    filename_hires_inhom = sys.argv[1]
    filename_lowres_rhos = sys.argv[2:-1]
    outfile = sys.argv[-1]

    sd_hires, obstacle, _ = load_simdata_from_filename(filename_hires_inhom)
    sd_lowres, _, _ = load_simdata_from_filename(filename_lowres_rhos[0])

    varnames = [f.split("_")[2].split(".")[0] for f in filename_lowres_rhos]

    obstacle = sd_lowres.vars["obstacle"] = reduce(obstacle, 8) < 0.9999

    Ncol = 3
    Nrows = [4, 3]

    subs = [subplots(n, Ncol, figsize=(maxwidthin, min([n*6*cm, maxheightin])), sharex=True, sharey=True, constrained_layout=True) for n in Nrows]
    Nfig = Nrows[0]*3
    figs = [s[0] for s in subs]
    axes = np.array(list(chain(*[s[1] for s in subs])))

    ax = axes[0]
    x, y = sd_lowres.meshgrid(-1, -2)
    f = obstacle[-1,0]
    ax.pcolormesh(x, y, f, vmin=0, vmax=1, cmap="binary")
    ax.set_title(f"({sublabel(0)}) ground truth")

    P = np.nansum(f)
    N = np.nansum(~f)
    info = f"P {P: 5}\nN {N: 5}"
    ax.text(0.05, 0.95, info, color="k", horizontalalignment="left", verticalalignment="top", transform=ax.transAxes, usetex=False, fontname="monospace", fontsize=8)

    for i, ax, varname in zip(range(1, 100), axes[1:], varnames):
        label = label2formula(varname)
        algo = re.match(r".*rho(\w*)", varname).groups()[0]
        TP, FP, FN, TN, colouring = score(sd_lowres.vars[varname][sd_lowres.shape[0]//2:], obstacle)
        ax.pcolormesh(x, y, colouring, vmin=0, vmax=3, cmap=cmap)
        ax.set_title(f"({sublabel(i%Nfig)}) prediction using ${label}$")

        err = (FP+FN) / np.sum(np.isfinite(colouring))
        info = f"misclassified {format_significant_figures(err*100, 2)}%\n"
        info += f"invalid {np.sum(~np.isfinite(colouring))}\n"
        for label, value in [("TP", TP), ("FP", FP), ("FN", FN), ("TN", TN)]:
            info += f"{label}{value: 5}\n"

        ax.text(0.05, 0.95, info, color="w", horizontalalignment="left", verticalalignment="top", transform=ax.transAxes, usetex=False, fontname="monospace", fontsize=8)

    filenames = [re.sub(r"\.(\w{3})", f".{i}." + r"\1", outfile) for i in range(len(figs))]
    for (i, fig), filename in zip(enumerate(figs), filenames):
        add_colorbar(fig, fig.get_axes(), cmap=cmap, ticks=[0,1,2,3], ticklabels=["true\nnegative\n(TN)", "false\nnegative\n(FN)", "false\npositive\n(FP)", "true\npositive\n(TP)"], vmin=-0.5, vmax=3.5, location="right", shrink=0.7)
        fig.savefig(filename)
        if i == 0:
            fig.savefig(outfile)
