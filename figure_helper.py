#!/bin/env python3
'''
Set Matplotlib settings to use LaTeX
'''
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import ithildin as ith
import re
import string
import numpy as np

plt.rcParams.update({
    'text.usetex': True,
    'font.family': 'serif',
    'font.serif': ['Computer Modern Roman'],
    'axes.titlesize': 'small',
    'axes.titlelocation': 'left',
})

cm = 1/2.54 # inches per centimetre
mm = 0.1*cm
maxwidthin = 7.5
maxheightin = 8.75
dpi = 300
UMIN, UMAX = -0.25, 1.25

zoomlims = dict(
    pdl2D_10=((138,178),10),
    phasealpa_7=((42,45),5),
    phase_68=((71,57),5),
)
zoomlims = {k: ((x-r,x+r),(y-r,y+r)) for k, ((x,y), r) in zoomlims.items()}
zoomlocs = dict(
    pdl2D_10=(3, 2, 4),
    phasealpa_7=(1, 2, 4),
    phase_68=(3, 2, 4),
)

def load_simdata_from_filename(filename:str):
    parts = filename.split('.')[0].split('_')
    stem = '_'.join(parts[:2])
    varname = parts[2]
    sd = ith.SimData.from_stem(stem)
    return sd, sd.vars.get(varname, None), varname

def sublabel(i:int, j:int=None, Nj:int=None) -> str:
    if j is not None:
        assert(Nj is not None)
        i = Nj*i + j
    s = string.ascii_uppercase
    assert(i < len(s))
    return s[i]

def subplots(*args, sharex=False, sharey=False, units=True, set_aspect=True, **kwargs):
    fig, axes = plt.subplots(*args, dpi=dpi, sharex=sharex, sharey=sharey, squeeze=False, **kwargs)
    for a in axes[-1] if sharex and sharex != 'row' else axes.flatten():
        a.set_xlabel('$x$' + ( ' in mm' if units else '' ))
    for a in axes[:,0] if sharey and sharey != 'col' else axes.flatten():
        a.set_ylabel('$y$' + ( ' in mm' if units else '' ))
    axes = axes.flatten()
    for ax in axes:
        if set_aspect:
            ax.set_aspect(1)
        for a in [ax.xaxis, ax.yaxis]:
            a.set_major_locator(ticker.MaxNLocator(2, steps=[1,10]))
            # a.set_minor_locator(ticker.MaxNLocator(6))
            a.set_minor_locator(ticker.NullLocator())
    return fig, axes

modelabbr = {
        'Bueno-Orovio 2008 4 var': 'BOCF',
        'Bueno-Orovio 2008, 4 var': 'BOCF',
        'Aliev-Panfilov, continuous epsilon': 'AP',
        'Fenton-Karma 3 var MLRI': 'FK',
        'optical mapping data': 'OM',
}

algoabbr = dict(
    glat='glat',
    pg='cpg', mpg='cpg', cpg='cpg',
    rpg='rpg',
    cm='cm', cos='cm',
    ip='ipm', ipm='ipm',
    ph='phm', mph='phm',
    pc='pc',
    svf='svf',
    am='am', topo='am',
    dm='dm', dipole='dm',
    qm='qm', quadrupole='qm',
)

def label2formula(label:str):
    if len(label) == 1:
        return label
    elif re.match(r'^phi', label):
        label = re.sub(r'^phi', r'', label)
        phases = dict(arr='arr', act='act', skew='skew')
        if label in phases:
            label = phases[label]
            return f'\\phi^\\mathrm{{{label}}}'
        elif 'rho' in label:
            p, r = label.split('rho')
            return label2formula('rho'+r) + '(' + label2formula('phi'+p) + ')'
        elif 'sigma' in label:
            p, r = label.split('sigma')
            return label2formula('sigma'+r) + '(' + label2formula('phi'+p) + ')'
    elif re.match(r'^rho', label):
        label = re.sub(r'^rho', r'', label)
        label = algoabbr[label].upper()
        return f'\\rho^\\mathrm{{{label}}}'
    elif re.match(r'^sigma', label):
        label = re.sub(r'^sigma', r'', label)
        edge, label = label[0], label[1:]
        edge = ' zyx'[int(edge)]
        label = algoabbr[label].upper()
        return f'\\sigma^\\mathrm{{{label}}}'
    raise ValueError(f'No such label known! {label}')

def add_colorbar_u(*args, **kwargs):
    return add_colorbar(*args, ticks=[0, 1], vmin=UMIN, vmax=UMAX, **kwargs)

def add_colorbar(fig, axes, ticks:list[float], ticklabels:list[str]=None, cmap=None, vmin:float=None, vmax:float=None, significant_figures:int=None, **kwargs):
    ticks = sorted(ticks)

    if significant_figures is not None:
        ticks = signif(ticks, significant_figures)
        if ticklabels is None:
            ticklabels = [format_significant_figures(t, significant_figures) for t in ticks]

    sm = plt.cm.ScalarMappable(cmap=cmap, norm=plt.Normalize(
        vmin = ticks[0]  if vmin is None else vmin,
        vmax = ticks[-1] if vmax is None else vmax,
    ))
    sm.set_array([])

    cbar = fig.colorbar(sm, ax=axes, **kwargs)
    cbar.set_ticks(ticks)

    if ticklabels is not None:
        cbar.set_ticklabels(ticklabels)

    return cbar

def signif(x:np.ndarray, p:int) -> np.ndarray:
    'Round to `p` significant digits'
    x = np.asarray(x)
    x_positive = np.where(np.isfinite(x) & (x != 0), np.abs(x), 10**(p-1))
    mags = 10 ** (p - 1 - np.floor(np.log10(x_positive)))
    return np.round(x * mags) / mags

def format_significant_figures(x:float, p:int) -> str:
    'Format a number with `p` significant digits'
    if x < 0:
        return '-' + format_significant_figures(-x, p)

    e = np.log10(np.abs(x))
    if not np.isfinite(e):
        return "0"
    e = int(e) if e >= 0 else int(e-1)
    digits = str(int(x*10**(p-e-1)+0.5))
    if e >= -2 and e < 0:
        return '0.'+'0'*(-e-1)+digits
    elif e >= 0 and e < 3:
        if p -1 > e:
            return digits[:e+1]+'.'+digits[e+1:]
        else:
            return digits+'0'*(1+e-p)
    else:
        return digits[:1]+'.'+digits[1:]+'E'+str(e)

if __name__ == '__main__':
    print(format_significant_figures(0.123e-2, 2))
    print(format_significant_figures(0.123e-1, 2))
    print(format_significant_figures(0.123e0, 2))
    print(format_significant_figures(0.123e1, 2))
    print(format_significant_figures(0.123e2, 2))
    print(format_significant_figures(0.123e3, 2))
    print(format_significant_figures(0.123e4, 2))
