#!/bin/env python3
"""
Reduce resolution of a log file
"""
import ithildin as ith
import sys

if __name__ == "__main__":
    infile = sys.argv[1]
    N = int(sys.argv[2])
    outfile = sys.argv[3]

    log = ith.Log(infile)

    series, serialnr = outfile.split("_")[:2]
    log.origin = [log.origin[0], log.origin[1], log.origin[2] + log.deltas[2]*N/2, log.origin[3] + log.deltas[3]*N/2]
    log.simseries = series
    log.serialnr = int(serialnr)
    log.domainsize = [l//N for l in log.domainsize]
    log.dzyx = [dx*N for dx in log.dzyx]

    with open(outfile, "w") as f:
        f.write(log.yaml)
