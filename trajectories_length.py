#!/bin/env python3
"""
Plot the length of a PDL over time
"""
import ithildin as ith
import numpy as np
import matplotlib.pyplot as plt
import sys
from figure_helper import *
from trajectories_angle import read_pdl_trajectory

_ = sys.argv[0]
tras = sys.argv[1:-2:2]
tra_idxs = sys.argv[2:-2:2]
_ = sys.argv[-2]
outfile = sys.argv[-1]

fig, axes = subplots(1, len(tras), figsize=(maxwidthin, 6*cm), set_aspect=False)

for (i, ax), tra, tra_idx in zip(enumerate(axes), tras, tra_idxs):
    sd, _, _ = load_simdata_from_filename(tra)
    use_units = not "Aliev-Panfilov" in sd.log.modeltype
    dt, dz, dy, dx = sd.deltas

    times, betas, lengths, ells = read_pdl_trajectory(tra, tra_idx, dt=dt)

    ax.plot(times, lengths)

    ax.set_ylabel("length $L$" + (" in mm" if use_units else ""))
    _, ymax = ax.get_ylim()
    ax.set_ylim(0, ymax)

    ax.set_xlabel("time $t$" + (" in ms" if use_units else ""))
    xlim = ax.get_xlim()
    ax.set_xticks(xlim)
    ax.set_xticklabels([f"{int(x):d}" for x in xlim])

    abbr = modelabbr[sd.log.modeltype]
    ax.set_title(f"({sublabel(i)}) {abbr} {'experiment' if abbr == 'OM' else 'simulation'}")

plt.tight_layout()
fig.savefig(outfile)
