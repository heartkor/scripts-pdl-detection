#!/bin/env python3
"""
A figure showing steps to get to PDLs.
"""
from figure_helper import *
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes, mark_inset
from itertools import chain
import sys
import ithildin as ith

cmap = "Greens"
outfile = sys.argv[1]
filenames = [a for a in sys.argv[2:] if "sigma3" not in a]

Nrow = len(filenames)//3
Nrows = [4 for _ in range(0, Nrow+1, 4)]
Nrows[-1] = Nrow % 4
if Nrows[-1] == 0:
    Nrows[-1] = 4

subs = [subplots(n, 3, figsize=(maxwidthin, min([n*6*cm, maxheightin])), sharex=True, sharey=True) for n in Nrows]
Nfig = Nrows[0]*3
figs = [s[0] for s in subs]
axes = np.array(list(chain(*[s[1] for s in subs])))

for ((i, ax), filename) in zip(enumerate(axes), filenames):
    print(f"{outfile}: {filename}")
    sd, u, varname = load_simdata_from_filename(filename)
    label = label2formula(varname)

    it = -20
    x, y = sd.linspace(-1), sd.linspace(-2)

    textcolor = "k"
    if "u" in varname and u[it,0,int(0.95*u.shape[2]),int(0.05*u.shape[2])] < 0.5:
        textcolor = "w"

    # ax.text(0.05, 0.95, f"({sublabel(i%Nfig)}) ${label}$", horizontalalignment="left", verticalalignment="top", transform=ax.transAxes, c=textcolor)
    ax.set_title(f"({sublabel(i%Nfig)}) ${label}$")

    if sd.log.stem in zoomlims:
        lims = zoomlims[sd.log.stem]
        locs = zoomlocs[sd.log.stem]
        axins = zoomed_inset_axes(ax, 5, loc=locs[0])
        axins.set_xlim(lims[0])
        axins.set_ylim(lims[1])
        axins.set_xticks([])
        axins.set_yticks([])
        mark_inset(ax, axins, loc1=locs[1], loc2=locs[2], fc="none", ec=(0.5, 0.5, 0.5, 1.), lw=0.5)

        subaxes = [ax, axins]
    else:
        subaxes = [ax]

    mask = sd.mask[0]

    vmin = 0
    vmax = 0
    ufr = u[it, 0]
    if "sigma" in varname:
        sigmax = re.sub(r"sigma[23]", r"sigma3", varname)
        sigmay = re.sub(r"sigma[23]", r"sigma2", varname)
        sigmax = np.where(mask, sd.vars[sigmax][it,0], np.nan)
        sigmay = np.where(mask, sd.vars[sigmay][it,0], np.nan)
        vmax = max(vmax, np.nanmax(sigmax), np.nanmax(sigmay))
        for a in subaxes:
            ith.plot.pcolordiagmesh(x, y, sigmay, sigmax, ax=a, vmin=vmin, vmax=vmax, cmap=cmap)
        add_colorbar(ax.get_figure(), ax, ticks=[vmin, vmax], cmap=cmap, significant_figures=2)
    elif "rho" in varname:
        vmax = max(vmax, np.nanmax(ufr))
        for a in subaxes:
            a.pcolormesh(x, y, np.where(mask, ufr, np.nan), vmin=vmin, vmax=vmax, cmap=cmap, shading="nearest")
        add_colorbar(ax.get_figure(), ax, ticks=[vmin, vmax], cmap=cmap, significant_figures=2)
    else:
        vmin, vmax = UMIN, UMAX
        for a in subaxes:
            a.pcolormesh(x, y, np.where(mask, ufr, np.nan), vmin=vmin, vmax=vmax, shading="nearest")
        add_colorbar_u(ax.get_figure(), ax)

    ax.set_xlim(x[0], x[-1])
    ax.set_ylim(y[0], y[-1])

filenames = [re.sub(r"\.(\w{3})", f".{i}." + r"\1", outfile) for i in range(len(figs))]
for (i, fig), filename in zip(enumerate(figs), filenames):
    fig.tight_layout()
    fig.savefig(filename)
    if i == 0:
        fig.savefig(outfile)
