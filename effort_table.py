#!/bin/env python3
"""
Convert effort measurements into LaTeX table
"""
import sys
import yaml

d = yaml.safe_load(open(sys.argv[1]))

print(f"""
\\caption{{Overview of existing and proposed algorithms for PD detection. Calculation times are for {d["Nt"]} frames in a medium of ${d["Nx"]}\\times {d["Ny"]}$ pixels on a Intel Core i7-10875H processor in a Numpy implementation without specific optimization for speed.}}
\\begin{{tabular}}{{lp{{40mm}}p{{60mm}}ccr}}
\\hline
\\textbf{{abbr.}} & \\textbf{{name}} & \\textbf{{rationale}} & \\textbf{{on vertices}} & \\textbf{{on edges}} & \\textbf{{calc. time}}\\\\
\\hline
CM & Cosine Method & Jumps in phase mod $2\\pi$ are ok, inbetween: PDL. & & \\checkmark & \\SI{{{d["sigma cos"]:.2f}}}{{\\second}} \\\\
GLAT & Gradient of Local Activation Time & LAT jumps at PDL and WF. & & \\checkmark & \\SI{{{d["sigma glat"]:.2f}}}{{\\second}} \\\\
RPG & Real Phase Gradient & Phase jumps at PDL. & & \\checkmark & \\SI{{{d["sigma rpg"]:.2f}}}{{\\second}} \\\\
CPG & Complex Phase Gradient & Phase jumps at PDL. & & \\checkmark & \\SI{{{d["sigma cpg"]:.2f}}}{{\\second}} \\\\
PC & Phase Coherence & Phase is not coherent at PDL. & & \\checkmark & \\SI{{{d["sigma pc"]:.2f}}}{{\\second}} \\\\
DM & Dipole Moment & Points on opposite side of PDL have opposite ``charge". & \\checkmark & & \\SI{{{d["rho di"]:.2f}}}{{\\second}} \\\\
SVF & Spatial Vector Field & Rotation of gradient is close to zero, except at PDL. & \\checkmark & & \\SI{{{d["rho svf"]:.2f}}}{{\\second}} \\\\
AM & Angular Momentum & Use the classical topological charge also known as angular momentum. & \\checkmark & & \\SI{{{d["rho topo"]:.2f}}}{{\\second}} \\\\
IPM & Inflection Point Method & The edge based extension to the PHM & & \\checkmark & \\SI{{{d["sigma ip"]:.2f}}}{{\\second}} \\\\
\\hline
\\end{{tabular}}
""")
